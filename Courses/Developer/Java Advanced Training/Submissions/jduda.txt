(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): Lynda.com
(Course Name): Java Advanced Training
(Course URL) : http://www.lynda.com/Java-tutorials/Java-Advanced-Training/107061-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): When are static initializer blocks run?
(A): Every time a static field in the class is assigned.
(B): Whenever you call a static method after the class is instantiated.
(C): Only the first time a static field of the class is assigned.
(D): Every time the class is instantiated.
(E): Only the first time garbage collection is run.
(Correct): C
(Points): 1
(CF): Static initializers are only run once - the first time any of the following happen: an instance of the class is created, a static method of the class is invoked, a static field of the class is assigned, or a non-constant static field is used.
(WF): Static initializers are only run once - the first time any of the following happen: an instance of the class is created, a static method of the class is invoked, a static field of the class is assigned, or a non-constant static field is used.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is a benefit of using local inner classes?
(A): Overloading.
(B): Inheritance.
(C): Polymorphism.
(D): Encapsulation.
(E): Synchronization.
(Correct): D
(Points): 1
(CF): Local inner classes help keep code clean and organized with encapsulation.
(WF): Local inner classes help keep code clean and organized with encapsulation.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): An enum constructor must be private.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Enum constructors must be private because you can not invoke them yourself.
(WF): Enum constructors must be private because you can not invoke them yourself.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What happens if you try to add an object that already exists in a set into that set?
(A): You will get a compilation error.
(B): You will get a runtime error.
(C): The object will be added to the set as a second copy.
(D): It negates the object already in the set, thus removing it.
(E): The add is ignored and nothing happens.
(Correct): E
(Points): 1
(CF): Sets require each object to be unique. If you try to add an existing object into the set a second time, it will be ignored. There will not be any errors.
(WF): Sets require each object to be unique. If you try to add an existing object into the set a second time, it will be ignored. There will not be any errors.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): You are getting a syntax error that you cannot add an object to a TreeSet. What must the object implement to be addable?
(A): Runnable.
(B): Comparable.
(C): TreeSet.
(D): Set.
(E): Class.
(Correct): B
(Points): 1
(CF): For a TreeSet to know how to order objects, the objects must implement Comparable.
(WF): For a TreeSet to know how to order objects, the objects must implement Comparable.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): After running the working code snippet below how many threads will your program be running on? (Not counting background threads for garbage collection, etc.)<br> new Thread(runnableA).start(); <br>new Thread(runnableB).start();
(A): 1.
(B): 2.
(C): 3.
(D): 4.
(E): 5.
(Correct): C
(Points): 1
(CF): There will be three threads running. The main one and then the two threads that were manually started.
(WF): There will be three threads running. The main one and then the two threads that were manually started.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)