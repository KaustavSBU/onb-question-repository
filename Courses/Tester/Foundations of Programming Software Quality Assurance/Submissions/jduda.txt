(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): lynda.com
(Course Name): Foundations of Programming: Software Quality Assurance
(Course URL): http://www.lynda.com/Developer-Programming-Foundations-tutorials/Foundations-Programming-Software-Quality-Assurance/126119-2.html
(Discipline): Technical
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): White box refers to...
(A): Having the customers do the testing.
(B): Testing from a remote server.
(C): Testing through the user interface.
(D): Testing at the code level.
(E): Waiting until a product is complete to do the testing.
(Correct): D
(Points): 1
(CF): White box testers test under the hood at the code level.
(WF): White box testers test under the hood at the code level.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Bugs that are found and fixed early are less costly to address.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Test early and test often is a motto software engineers try to abide by.
(WF): Test early and test often is a motto software engineers try to abide by.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Quality assurance is in place to...
(A): Determine the goals and requirements for a product.
(B): Explain the product goals to the customer.
(C): Ensure a product is meeting its goals.
(D): Decide how to fulfill the goals of a product.
(E): All of the above.
(Correct): C
(Points): 1
(CF): Product management should determine the goals of a product. Developers will determine how those goals will be met. QA ensure that the goals actually are being met.
(WF): Product management should determine the goals of a product. Developers will determine how those goals will be met. QA ensure that the goals actually are being met.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Black box testing does not use automation.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Black box testing can use automation. For example, the tester may want to use an automated bitmap comparison to simulate a user clicking buttons.
(WF): Black box testing can use automation. For example, the tester may want to use an automated bitmap comparison to simulate a user clicking buttons.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Grey box testers are often transitioning from...
(A): White box testers to black box testers.
(B): Trainees to black box testers.
(C): Black box testers to white box testers.
(D): Novices to adepts.
(E): Developers to testers.
(Correct): C
(Points): 1
(CF): Grey box refers to black box testers that begin transitioning to white box testers by being more aware of what is going on under the hood and learning the technical skills to understand it.
(WF): Grey box refers to black box testers that begin transitioning to white box testers by being more aware of what is going on under the hood and learning the technical skills to understand it.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): After development fixes a bug, it is important that the original bug reporter verifies the software works correctly.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The original bug reporter is in the best position for replicating and determining if a bug is fixed.
(WF): The original bug reporter is in the best position for replicating and determining if a bug is fixed.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Quality is about achieving absolute perfection.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Quality is understanding what you realistically can and hope to accomplish, and working to ensure that. 100% perfection is generally not realistic.
(WF): Quality is understanding what you realistically can and hope to accomplish, and working to ensure that. 100% perfection is generally not realistic.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Automation is best used when...
(A): You want to completely eliminate human involvement in the testing.
(B): You want to optimize your process to make testing more efficient.
(C): The product functionality is frequently being updated.
(D): The test only needs to be run once.
(E): You have minimal knowledge on what the expected outputs of the test are.
(Correct): B
(Points): 1
(CF): Automation should be used to optimize testing. It does not mean there should be no human participation. There will be a high maintenance cost if the product functionality is not stable. D and E would not be a good use of resources.
(WF): Automation should be used to optimize testing. It does not mean there should be no human participation. There will be a high maintenance cost if the product functionality is not stable. D and E would not be a good use of resources.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): It is important to rank bugs by severity and discoverability. Based on the video, which bugs below might be deemed critical?
(A): Mobile users (about 10% of customers) report a crash whenever they get receive a phone call.
(B): A handful of users have complained that the font is too large and wastes a lot of screen space.
(C): The program crashes for more than 50% of users when they click a button.
(D): About 65% of users have complained that the save and load buttons are in the wrong order.
(E): 80% of users have reported that the print button does nothing at all.
(Correct): A, C, E
(Points): 1
(CF): Based on the video, crashes of any type would be critical (A, C). Non-functioning features that affect many users are also high priority (E). Cosmetic features are generally not high priority (B, D).
(WF): Based on the video, crashes of any type would be critical (A, C). Non-functioning features that affect many users are also high priority (E). Cosmetic features are generally not high priority (B, D).
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): What kind of information should a bug base track?
(A): The frequency of which bugs occur.
(B): The steps required to replicate a bug.
(C): Target fix date for the bug.
(D): Build information on the version in which the bug occurred.
(E): The bug priority level.
(Correct): A, B, C, D, E
(Points): 1
(CF): All of the above is important information that should be recorded.
(WF): All of the above is important information that should be recorded.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Your software has become popular and the user-base is quickly growing. You are worried that one day there will be too many users and want to find out what will happen when the software fails. What test should you perform?
(A): Load test.
(B): Performance test.
(C): Stress test.
(D): Usability test.
(E): It is not a testable situation. You have to wait until there is actually too many users.
(Correct): C
(Points): 1
(CF): Stress tests are designed to determine the maximum number of users possible and what happens when the program fails. Load tests measure how a program responds with different numbers of concurrent users. Performance tests measure the time operations take to complete under different kinds of load.
(WF): Stress tests are designed to determine the maximum number of users possible and what happens when the program fails. Load tests measure how a program responds with different numbers of concurrent users. Performance tests measure the time operations take to complete under different kinds of load.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): When using automation, why should you try to avoid using bitmap comparisons whenever possible?
(A): Bitmaps are not a universal format.
(B): Bitmaps take up a lot of memory.
(C): Bitmaps are prone to corruption.
(D): Bitmaps tend to have a huge maintenance cost.
(E): Bitmaps ignore the text on the screen.
(Correct): D
(Points): 1
(CF): Bitmap comparisons have a huge maintenance cost because minor changes in the GUI usually requires them to be rewritten. Other factors such as resolution and RGB color scheme can also affect them.
(WF): Bitmap comparisons have a huge maintenance cost because minor changes in the GUI usually requires them to be rewritten. Other factors such as resolution and RGB color scheme can also affect them.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): You are developing a PC video game that will be shipped to stores around the world. Which of the below are important user scenarios that should be tested? (Choose 3)
(A): Install and uninstall situations.
(B): Disabled users that use alternative input methods.
(C): The users favorite web browser.
(D): What time of day the user will be playing the game.
(E): Multiple languages and locales where the software will be used.
(Correct): A, B, E
(Points): 1
(CF): It is important to ensure all aspects of a product work, not just the core application. A, B, and E all represent common user scenarios that should be tested. C and D should not affect how the software works.
(WF): It is important to ensure all aspects of a product work, not just the core application. A, B, and E all represent common user scenarios that should be tested. C and D should not affect how the software works.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): What is NOT true about white-box testers?
(A): They should be writing unit tests.
(B): They are generally more technical than black-box testers.
(C): They are not concerned with the UI or usability.
(D): They work at the code level.
(E): They help build the test infrastructure.
(Correct): A
(Points): 1
(CF): Unit tests should be written by developers. While a white-box tester could write unit tests if they fully understand the code, it is not best practice.
(WF): Unit tests should be written by developers. While a white-box tester could write unit tests if they fully understand the code, it is not best practice.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): A bug model is useful for...
(A): Plan ahead for time to fix bugs.
(B): Recording the specific steps to reproduce a bug.
(C): Storing a list of all the bug records.
(D): Giving an educated guess on how many bugs you will be tracking at a given step.
(E): Tracking test cases and their results.
(Correct): A, D
(Points): 1
(CF): A and D are both reasons to use a bug model. The information for B, C, and E should be stored in a different type of tracking system.
(WF): A and D are both reasons to use a bug model. The information for B, C, and E should be stored in a different type of tracking system.
(STARTIGNORE)
(Hint):
(Subject): Software Quality Assurance
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)