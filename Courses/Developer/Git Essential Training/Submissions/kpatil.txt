(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Kamalesh Patil
(Course Site): lynda.com
(Course Name): Git Essentials
(Course URL): http://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 0
(Question): Executing the command 'git branch purge' results in -
(A): deletion of current branch.
(B): deletion of all untracked as well as uncommitted changes in current branch.
(C): creation of a branch named "purge". 
(D): None of the above
(Correct): C
(Points): 1
(CF): The string "purge" is not a flag/option for the 'git branch' command and so it is treated as a value for the branch name parameter.
(WF): The string "purge" is not a flag/option for the 'git branch' command and so it is treated as a value for the branch name parameter.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): The command 'git log --grep="[3-5] days"' shows commit log between last 3 and 5 days - inclusive.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The "--grep" flag/option is used for regular expression search within the commit log, so the string "[3-5] days" is treated as a regular expression as opposed to a date range.
(WF): The "--grep" flag/option is used for regular expression search within the commit log, so the string "[3-5] days" is treated as a regular expression as opposed to a date range.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which one of the following is a good strategy to avoid merge conflicts?
(A): Branch often
(B): Push often
(C): Fetch often
(D): Merge often
(Correct): D
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
