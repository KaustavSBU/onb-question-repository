(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): lynda.com
(Course Name): Designing a Presentation
(Course URL): http://www.lynda.com/Keynote-tutorials/Designing-Presentation/124082-2.html
(Discipline): Professional
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Microsoft PowerPoint 2013 and above easily enable sharing presentations in which of the formats below?
(A): As a movie.
(B): As a PDF file.
(C): As a group of images, one per slide.
(D): As an editable PPT file.
(Correct): A,B,C,D
(Points): 1
(CF): All of the above are valid output formats for a PowerPoint presentation.
(WF): All of the above are valid output formats for a PowerPoint presentation.
(STARTIGNORE)
(Hint):
(Subject): PowerPoint
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): A PowerPoint presentation will be shown in Presenter View automatically if dual monitors are being used.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Microsoft PowerPoint will automatically detect the view mode.
(WF): Microsoft PowerPoint will automatically detect the view mode.
(STARTIGNORE)
(Hint):
(Subject): PowerPoint
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which feature would you use to make your own custom slide layouts within a theme?
(A): Template Tackler.
(B): Layout Designer.
(C): Slide Master
(D): Slide Editor.
(E): You can only load premade layouts.
(Correct): C
(Points): 1
(CF): The Slide Master allows editing and saving of custom slide layouts.
(WF): The Slide Master allows editing and saving of custom slide layouts.
(STARTIGNORE)
(Hint):
(Subject): PowerPoint
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): You can present a PowerPoint presentation live over the internet.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): This can be done by going to File...Share...Present Online.
(WF): This can be done by going to File...Share...Present Online.
(STARTIGNORE)
(Hint):
(Subject): PowerPoint
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What are some features of presenter mode?
(A): Ability to draw on slides.
(B): Ability to live edit content.
(C): Preview of the next slide in the queue.
(D): Ability to zoom in and out.
(E): Ability to overlay slide notes so the audience can see them.
(Correct): A, C, D
(Points): 1
(CF): Presenter mode has many useful features, but live editing content and overlaying notes are not possible.
(WF): Presenter mode has many useful features, but live editing content and overlaying notes are not possible.
(STARTIGNORE)
(Hint):
(Subject): PowerPoint
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)