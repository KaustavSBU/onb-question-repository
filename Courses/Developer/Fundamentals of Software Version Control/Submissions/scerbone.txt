(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): lynda.com
(Course Name):Fundamentals of Software Version Control
(Course URL): http://www.lynda.com/Version-Control-tutorials/Fundamentals-Software-Version-Control/106788-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%2Bsoftware%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): What does version control help with?
(A): Backing up work
(B): Keeping a history of work
(C): Easy to remember naming conventions
(D): Experimenting in a "sandbox"
(E): Friction free undo
(Correct): A, B, D, E
(Points): 1
(CF): Version control helps with backing up work, keeping a history of work, friction free undo, and the ability to "sandbox".
(WF): Version control helps with backing up work, keeping a history of work, friction free undo, and the ability to "sandbox".
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): What are key version control concepts?
(A): Repository
(B): Tree
(C): Branch
(D): Working set
(E): Encryption
(Correct): A, C, D
(Points): 1
(CF): Key version control concepts are a repository, branch, and working set.
(WF): Key version control concepts are a repository, branch, and working set.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): A working set is the list of new/altered files on the main repository (or trunk).
(A): True
(B): False 
(Correct): B
(Points): 1
(CF): A working set is the list of new/altered files on the local machine.
(WF): A working set is the list of new/altered files on the local machine.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Every commit must have a message.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Every commit must have a message, this explains what changes were made and why.
(WF): Every commit must have a message, this explains what changes were made and why.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): What is integrating a branch into a repository called?
(A): Merging
(B): Reverse Integration
(C): Forward Integration
(D): Branching
(E): Adding
(Correct): A, B
(Points): 1
(CF): Integrating a branch into a repository is called merging or reverse integration.
(WF): Integrating a branch into a repository is called merging or reverse integration.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Checking out a file always locks it until it is checked back in.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Checking out a file sometimes locks the file until it is checked back in to avoid conflicts.
(WF): Checking out a file sometimes locks the file until it is checked back in to avoid conflicts.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Which version control option integrates git directly into the work flow?
(A): Command line
(B): GUI
(C): IDE
(D): Shell
(E): GitHub
(Correct): C
(Points): 1
(CF): When version control is used with an IDE, it is integrated into the work flow.
(WF): When version control is used with an IDE, it is integrated into the work flow.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): When version control is centralized it is often more fragile.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When version control is centralized it is often more fragile.
(WF): When version control is centralized it is often more fragile.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Instead of using push/pull, in order to keep a change private, one must
(A): export/import.
(B): branch/merge.
(C): fork/merge.
(D): commit/add
(E): 
(Correct): A
(Points): 1
(CF): Using export/import allows changes to be emailed securely, thus kept private.
(WF): Using export/import allows changes to be emailed securely, thus kept private.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): A tag/label is the state of a repository.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): A tag/label is the state of a repository.
(WF): A tag/label is the state of a repository.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)