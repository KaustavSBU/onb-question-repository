(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):Nikita Padmannavar
(Course Site):lynda.com
(Course Name):SQL Essential Training
(Course URL):http://www.lynda.com/SQL-tutorials/Goodbye/139988/166317-4.html
(Discipline):Technical
(ENDIGNORE)

(Type): multipleselect
(Category): 21
(Grade style): 1
(Random answers): 1
(Question): Which of the following can be used to filter data? (choose 3)
(A): LIKE
(B): ORDER BY
(C): IN
(D): HAVING
(Correct): A, C, D
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):21
(Grade style): 0
(Random answers): 1
(Question): What will be the result of 'ORDER BY a desc, b, c;' Here a,b,c are columns.
(A): Sorting order: a, b, c all in descending order
(B): Sorting order: first c ascending, then a and b descending
(C): Sorting order: first a descending, then b and c ascending
(D): Sorting order: first c ascending, then b ascending, then a descending
(Correct): C
(Points): 1
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):21
(Grade style): 0
(Random answers): 1
(Question): CASE expression is a conditional expression.
(A): True
(B): False
(Correct): A
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):21
(Grade style): 0
(Random answers): 1
(Question): How can a single quote be escaped in a string literal?
(A): Use / before the single quote in the string literal
(B): Use * before the single quote in the string literal
(C): Use ^ before the single quote in the string literal
(D): Use another single quote before the single quote in the string literal
(Correct): D
(Points): 1
(CF): 'Here''s an example'
(WF): 'Here''s an example'
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):21
(Grade style): 0
(Random answers): 1
(Question): Decimal datatype is same as float datatype.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Decimals are fixed precision numbers with decimal point which are not real or floating point numbers.
(WF): Decimals are fixed precision numbers with decimal point which are not real or floating point numbers.
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category):21
(Grade style): 0
(Random answers): 1
(Question): When DISTINCT is used with the aggregate functions,
(A): DISTINCT is executed first and then the aggregate function
(B): Aggregate function is executed first and then DISTINCT
(C): Any order works the same
(D): None of the above
(Correct): A
(Points): 1
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category):21
(Grade style): 1
(Random answers): 1
(Question): Why is using tansactions better? (choose 2)
(A): System confirms to every data written to the database
(B): Improves reliability
(C): Improves performance
(D): None of the above
(Correct): B, C
(Points): 1
(CF): Improves reliability and performance because the system need not confirm to every data written to the database.
(WF): Improves reliability and performance because the system need not confirm to every data written to the database.
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):21
(Grade style): 0
(Random answers): 1
(Question): Result of a SELECT statement is a table.
(A): True
(B): False
(Correct): A
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):21
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a string function?
(A): UPPER
(B): TRIM
(C): CONCAT
(D): ROUND
(Correct): D
(Points): 1
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):21
(Grade style): 0
(Random answers): 1
(Question): TRIGGERS can be used only after a change has been made on a table.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): TRIGGERS can be used after a change has been made on a table or before a change is initiated.
(WF): TRIGGERS can be used after a change has been made on a table or before a change is initiated.
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)