(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tsetsen erdene Ganbaatar
(Course Site): Udemy
(Course Name): Communication Skills
(Course URL): https://www.udemy.com/consulting-skills-series-communication/
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question):  Which one of these are NOT part of Non-verbal Communication?
(A): Facial Expressions
(B): Gestures
(C): Posture
(D): First Impression
(E): Paralinguistics
(Correct): D
(Points): 1
(CF): All points except for First Impression are integral aspects of non verbal communication.
(WF): All points except for First Impression are integral aspects of non verbal communication.
(STARTIGNORE)
(Hint):
(Subject): People
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When planning for a conference call, which one of these should you NOT do?
(A): Have a purpose
(B): Have an expected result
(C): Create and Distribute the agenda.
(D): Email all attendees that haven't RSVP'd yet and CC their boss to hold the attendee accountable to their boss.
(E): Ask yourself "Do I need to organize this call"
(Correct): D
(Points): 1
(CF): Correct!
(WF): Do not spam your attendees and ESPECIALLY do not spam their boss
(STARTIGNORE)
(Hint):
(Subject): Conference Calls
(Difficulty):Intermediate
(Applicability):Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): If you are not sure who the chair of the meeting is, who should you address?
(A): No one. It's understandable that you do not know.
(B): Your closest supervisor in the meeting, as they are ultimately responsible for you.
(C): The most influential person in the room.
(D): The person that distributed the agenda.
(E): Yourself, as you must be the chair if no one else steps up for it.
(Correct): D
(Points): 1
(CF): Correct.
(WF): The Course instructor recommends that you address the person who handed out the agenda if you do not know who the chair is.
(STARTIGNORE)
(Hint):
(Subject): Meetings
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When someone is talking during a meeting, you should square up to them
(A): True
(B): False
(Correct): True
(Points): 1
(CF): When someone is speaking, make an effort to face them.
(WF): When someone is speaking, make an effort to face them.
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What does "check your ninja skills at the door" mean?
(A): Don't bring melee weapons into meetings.
(B): Act with honor and dignity, just like a ninja.
(C): Act with grace and patience, just like a ninja.
(D): Keep your ego in check. You don't have to prove that you're better than anyone.
(E): Be yourself, don't skulk as if you are someone else.
(Correct): D
(Points): 1
(CF): Correct. Using ninja skills constantly can emotionally intimidate everyone else in the meeting.
(WF): Ninja skills refer to being over-confident and needing to prove that you are the smartest person in the room.
(STARTIGNORE)
(Hint):
(Subject): Meetings
(Difficulty): Beginner.
(Applicability): Course
(ENDIGNORE)
