(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Victor Oyedeji
(Course Site): Lynda
(Course Name): Presentation Fundamentals
(Course URL): http://www.lynda.com/Business-Business-Skills-tutorials/Presentation-Fundamentals/151544-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which is not part of a Compelling presentation?
(A): Research on the object
(B): Research on the topic
(C): Research on the audience
(D): Research on the presentation tools
(Correct): D
(Points): 1
(CF): Research on the presentation tools may be important, however it's not a requisite of a compelling presentation.
(WF): Research on the presentation tools may be important, however it's not a requisite of a compelling presentation.
(STARTIGNORE)
(Hint):
(Subject): Presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which is NOT a fundamental question a presenter should ask themselves while preparing for their audience?
(A): What do they know about the topic?
(B): What do they not know about the topic?
(C): How many questions will they ask once I'm done presenting?
(Correct): C
(Points): 1
(CF): "How many questions will they ask once I'm done presenting" is not a fundamental question to ask during preparations.
(WF): "How many questions will they ask once I'm done presenting" is not a fundamental question to ask during preparations.
(STARTIGNORE)
(Hint):
(Subject): Presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): How should a presenter present a topic to an audience about a topic they are unfamiliar with?
(A): Relate it something they do know, then help them connect the dots.
(B): Detail the topic on the slides during your presentation.
(C): Ask them during the presentation what they do know.
(D): Skip the topic, and come back to it after the presentation.
(E): Detail the topic more than others during your presentation.
(Correct): A
(Points): 1
(CF): Relate it something they do know, then help them connect the dots.
(WF): Relate it something they do know, then help them connect the dots.
(STARTIGNORE)
(Hint):
(Subject): Presentation Planning
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): When does a presenter use "insider language" during a presentation?
(A): When the presenter feels comfortable speaking to the audience.
(B): Whenever the presenter gets approval from their superior.
(C): When the audience begins to daydream during your speech.
(D): When the audience is familiar with the company inner-workings.
(E): When the presenter gets an "insider tip" from their research.
(Correct): D
(Points): 1
(CF): When the audience is familiar with the company inner-workings, the presenter can use "insider language" during their presentation.
(WF): When the audience is familiar with the company inner-workings, the presenter can use "insider language" during their presentation.
(STARTIGNORE)
(Hint):
(Subject): Presentation Delivery
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What does a presenter do when presenting in front of a group that knows more about a topic than that person?
(A): Don't say anything. 
(B): Act like you know more than that group.
(C): Recognize them, and use them as part of your presentation.
(D): Talk to them before your presentation and gather as much knowledge as possible
(Correct): C
(Points): 1
(CF): Recognize the group, and use them as part of your presentation whenever a presenter is presented with that scenario.
(WF): Recognize the group, and use them as part of your presentation whenever a presenter is presented with that scenario.
(STARTIGNORE)
(Hint):
(Subject): Presentation 
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 0
(Question): When does a presenter address the audience's values ("what's in it for them") during a presentation?
(A): At the Beginning of the presentation
(B): In the Middle of the presentation
(C): During the closing
(D): During the Q&A segment
(Correct): A
(Points): 1
(CF): A presenter addresses the audience's values at the Beginning of their presentation.
(WF): A presenter addresses the audience's values at the Beginning of their presentation.
(STARTIGNORE)
(Hint):
(Subject): Presentation 
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What type of presentation would one give to an audience that is just learning about the company for the very time?
(A): Forest
(B): Trees
(C): Desert
(D): Land
(E): Grass
(Correct): A
(Points): 1
(CF): A "Forest" presentation deals with a general picture ("Trees" presentation deals with details)
(WF): A "Forest" presentation deals with a general picture ("Trees" presentation deals with details)
(STARTIGNORE)
(Hint):
(Subject): Presentation 
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 0
(Question): What robs a speaker of his/her credibility from the audience, is using fillers ("ummm", "uhhh", "so..") during a presentation.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): This statement is true.  Practice at your own time to avoid these fillers.
(WF): This statement is true.  Practice at your own time to avoid these fillers
(STARTIGNORE)
(Hint):
(Subject): Presentation Delivery
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Albert Mehrabian discovered that ______ of a message is carried by non-verbal�s.
(A): 90%
(B): 93%
(C): 83%
(D): 50%
(E): 23%
(Correct): B
(Points): 1
(CF): Albert Mehrabian discovered that 93% of a message is carried by non-verbal�s.
(WF): Albert Mehrabian discovered that 93% of a message is carried by non-verbal�s.
(STARTIGNORE)
(Hint):
(Subject): Presentation Delivery
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): First impressions are made between the first 7-17 seconds of any interaction.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): This statement is true...and very important when conducting a presentation.
(WF): This statement is true...and very important when conducting a presentation.
(STARTIGNORE)
(Hint):
(Subject): Presentation Delivery
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which level of influence deals with "credibility appeal"?
(A): Ethos
(B): Logos
(C): Pathos
(Correct): A
(Points): 1
(CF): Ethos level of influence deals with "credibility appeal".
(WF): Ethos level of influence deals with "credibility appeal".
(STARTIGNORE)
(Hint):
(Subject): Presentation Delivery
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
