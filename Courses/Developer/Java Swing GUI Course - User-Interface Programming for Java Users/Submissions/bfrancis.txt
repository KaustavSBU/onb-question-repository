(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): lynda.com
(Course Name): Java Swing GUI Training
(Course URL): https://www.udemy.com/java-swing-complete/?dtcode=MqxESoN1GfPr
(Discipline): Technical
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): In swing how to you capture events to act upon them?
(A) You implement some kind of a listener to capture events and act upon them.
(B) The main class will automatically capture them for you
(C) Swing is not event driven so you don't do anything
(D) You capture it in the main class
(Correct): A
(Points): 1
(CF): To capture swing events you need to implement a listener.
(WF): To capture swing events you need to implement a listener.
(STARTIGNORE)
(Hint):
(Subject): Java swing GUI training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What type of layout in swing places items from left to right across the screen?
(A) RightToLeftLayout
(B) GridBagLayout
(C) FlowLayout
(D) FollowerLayout
(Correct): C
(Points): 1
(CF): A flow layout will display items from left to right of the screen.
(WF): A flow layout will display items from left to right of the screen.
(STARTIGNORE)
(Hint):
(Subject): Java swing GUI training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What type of layout has a north, south, east and west quadrant?
(A) CompassLayout
(B) GridBagLayout
(C) FlowLayout
(D) DirectionalLayout
(Correct): B
(Points): 1
(CF): A grid bag layout uses different quadrants across the screen like north, south, east, and west.
(WF): A grid bag layout uses different quadrants across the screen like north, south, east, and west.
(STARTIGNORE)
(Hint):
(Subject): Java swing GUI training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What type of architecture is swing based on?
(A) Factory Pattern
(B) MVC(Model View Controller)
(C) Adapter Pattern
(D) Java Swing Pattern
(Correct): B
(Points): 1
(CF): Swing is largely based on creating a model view controller design pattern.
(WF): Swing is largely based on creating a model view controller design pattern.
(STARTIGNORE)
(Hint):
(Subject): Java swing GUI training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Which is the most difficult component to customize and worth with in swing?
(A) Radio Buttons
(B) Grids
(C) Trees
(D) Regular Buttons
(Correct): C
(Points): 1
(CF): Of the components that were lectured on trees are by far the most difficult component to customize and work with in swing.
(WF): Of the components that were lectured on trees are by far the most difficult component to customize and work with in swing.
(STARTIGNORE)
(Hint):
(Subject): Java swing GUI training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
